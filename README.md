# Reflection in a Pond

This is an interactive website specifically made for my class' graduation online Art exhibition 2021. (June 3 to June 10) Reflection in a Pond is the name of our exhibition, where we realized the covid isolation has fostered our own inner persuit of some kind. We feel like we are constantly reflecting upon ourselves just like the moment Narcissus looked down on a steady pond seeing his own reflection. We see ourselves in things and people around us. This website is an effort to tell our journey to more people alongside with a physical exhibition that will be held at June 3 to June 10 at Bishop's court, Prague, Czech Republic.

The idea of the website is to layout html elements (contents about our artworks) flat on a really big canvas, and visitors of the website will have an avator spawned on this canvas, walking around and looking at the contents and also seeing other visitors walking around the space. p5 was used to render the ui, and sinatra websocket was used to make visitors been seen to each other.

[[_TOC_]]

## How to run the server?
First initialize a database for CMS logins by running any commands from admin.rb before starting the server. (See next session)  
Then start the server by running "rackup -p any_port_number" on the terminal.

## Content Management System
### admin logins
Run any commands from the admin.rb to initialize a database (a json file) for storing admin accounts using the content management system. For the simplest, run

```bash
ruby admin.rb status
```
### How to manage the admin logins?
run "ruby admin.rb method arguments"  

```bash
# Display a list of avaliable commands and their parameters
ruby admin.rb help
# Add admin account
ruby admin.rb add_admin <admin_name> <password>
# Delete admin account by id
ruby admin.rb delete_admin <id>
# Know the id by the name of the admin, return multiple results if there's more than one
ruby admin.rb get_id_by_name <admin_name>
# Delete all admin accounts
ruby admin.rb reset_all_admin
```

## Project Structure

```markdown
.  
|-- backend  
|   |-- app.rb: server routes, including receive websocket messages  
|   |-- client.rb: class for handling incoming websocket messages  
|   |-- world.rb: functions for keeping track at all clients and map items, and for broadcasting messages using websockets to every clients.  
|   |-- admin_db.rb: class for accessing login accounts for the cms  
|   |-- map_object.rb: class for making map objects  
|   |-- message_object.rb: class for constructing message objects  
|   |-- utils.rb: other generic functions, including scheduled tasks and recurring tasks that runs on seperate threads.  
|-- frontend  
|   |-- js  
|   |   |-- client.js: an object containing all the client's attributes and movements, including a listener that handles websocket messages upon receiving websocket messages from the server  
|   |   |-- message.js: message class for constructing messages (mapitem types will be eventually written in the backend)  
|   |   |-- ui.js: rendering of the ui  
|   |   |-- cms.js: rendering of the cms's ui  
|   |   |-- ws.js: js websocket implementation  
|   |   |-- p5.min.js: p5 library for rendering the ui  
|   |   |-- quill.min.js, quill.min.js.map, highlight.min.js: quill text editor  
|   |   |-- style.css  
|   |-- css  
|   |-- img  
|   |-- font  
|   |-- index.html  
|   |-- admin-login.html  
|   |-- cms.html  
|-- media: uploaded media will be saved here  
|-- backup: a directory for storing backups of the world every 10 minutes (this directory is generated automatically)  
|-- admin.rb: an admin utils program to manage the cms logins
|-- world.json: where all world objects are saved on the server
|-- config.ru  
|-- Gemfile  
|-- README.md  
```

## Message Types (constructed with javascript at frontend)
Implemented in message.js

<details><summary>Message types for every visitors including admins</summary>

```javascript
function ClientInitMsg(destX, destY, name) {
    this.type = 'clientInit';
    this.destX = destX;
    this.destY = destY;
    this.name = name;
}

function ClientMoveMsg(destX, destY) {
    this.type = 'clientMove';
    this.destX = destX;
    this.destY = destY;
}

function NameChangeMsg(name) {
    this.type = 'nameChange'; 
    this.name = name;
}
```
</details>

<details><summary>Message types for admins</summary>

```javascript
function AddMapObjectMsg(obj, fileData='') {
    this.type = 'addMapObject';
    this.obj = obj;
    this.fileData = fileData;
}

function UpdateMapObjectMsg(id, attr, newValue) {
    this.type = 'updateMapObject';
    this.id = id;
    this.attr = attr;
    this.newValue = newValue;
}

function DeleteMapObjectMsg(id) {
    this.type = 'deleteMapObject';
    this.id = id;
}
```
</details>


## Message Types (constructed with Ruby at backend)
Implemented in message_object.rb. But for clarity, I will document it in javascript:

```javascript
function MapChangeMsg(map) {
    this.type = 'mapChange';
    this.map = map; //a list of Messages that conveys the change of the map
    this.add_uid = (uid) => {this.uid = uid};
    this.to_hash = () => {/*a function that return current object as a Hash*/};
}

function ClientCloseMsg(uid) {
    this.type = 'clientClose';
    this.uid = uid;
    this.to_hash = () => {/*a function that return current object as a Hash*/};
}
```

## Map Item Types (constructed with Ruby at backend)
Implemented in map_object.rb. But for clarity, I will document it in javascript:

```javascript
function Visitor(uid, pos_x, pos_y, name) {
    this.type = 'visitor';
    this.uid = uid;
    this.pos = { x: pos_x, y: pos_y };
    this.name = name;
    this.to_hash = () => {/*a function that return current object as a Hash*/}; 
}

function Rect(uid, pos_x, pos_y, size_w, size_h, color) {
    this.type = 'rect';
    this.uid = uid;
    this.pos = { x: pos_x, y: pos_y };
    this.size = { w: size_w, h: size_h }; 
    this.color = color;
}
```

## Rendering Types (interpreted from the received Map Item Types, constructed with javascript at frontend)
Implemented in client.js. Whenever a visitor comes, the frontend will initiate a client object for the visitor. Note that the Visitor object is the superclass of the client object. So the client object inherits all functions from the visitor object and have extra functions for handling message received from the server, and render them as the following types, eventually appending to this.client.mapItems[], where every item inside the mapItems will be rendered with the p5 draw function each frame. (see the function renderMap in ui.js)

```javascript
function Visitor(posX, posY, uid="", name="new visitor") {
  this.type = 'visitor';
  this.uid = uid;
  this.name = name;
  this.pos = new p5.Vector(posX, posY);
  this.dir = new p5.Vector(0, 0);
  this.destination = this.pos;
  this.speed = 0;
  this.tick = () => {
    if (this.pos.dist(this.destination) > 1) {
      this.pos.add(p5.Vector.mult(this.dir, this.speed));
      this.dir = p5.Vector.sub(this.destination, this.pos).normalize();
      this.speed = this.pos.dist(this.destination) / 20;}};
}

function Rect(uid, posX, posY, sizeW, sizeH, color) {
    this.uid = uid;
    this.type = 'rect';
    this.pos = new p5.Vector(posX, posY);
    this.size = new p5.Vector(sizeW, sizeH);
    this.color = color;
}
```