#!/usr/local/opt/ruby/bin/ruby

require_relative 'backend/admin_db.rb'

class ServerAdminAccess < AdminDataBase

    Admin = Struct.new :id, :name, :password_hash

    def initialize file, hash
        super file, hash
    end

    # Load admin list from json
    def self.load_admin_list file
        File.open("world.json", "w") { |f| f.write "{}" } unless File.exist? 'world.json'
        if (File.exist? file) then
            admin_list_json = ''
            File.open(file) { |f| admin_list_json = f.read }
            return ServerAdminAccess.new file, JSON.parse(admin_list_json)
        else
            # Init the database for admin logins if the database(admin_list.json) is not found
            File.open(file, "w") { |f| f.write "{\"admins\":[]}" } 
            return ServerAdminAccess.new file, JSON.parse("{\"admins\":[]}")
        end
    end
      
    def add_admin admin_name, password
        @admin_list << Admin.new(generate_uid, admin_name, hash_password(password)).to_h
        admin_hash = {admins: @admin_list}
        File.open(@file, "w") { |f| f.write admin_hash.to_json }
    end

    def delete_admin id
        @admin_list.reject! {|admin| admin["id"] == id}
        admin_hash = {admins: @admin_list}
        File.open(@file, "w") { |f| f.write admin_hash.to_json }
    end
    
    def reset_pwd id, new_password
        @admin_list.collect! do |admin| 
            admin["password_hash"] = hash_password(new_password) if admin["id"] == id 
            admin
        end
        admin_hash = {admins: @admin_list}
        File.open(@file, "w") { |f| f.write admin_hash.to_json } 
    end

    def get_id_by_name admin_name
        @admin_list.each {|admin| puts "#{admin_name}'s id: #{admin["id"]}" if admin["name"] == admin_name }
    end
    
    def reset_all_admin
        @admin_list = []
        File.open(@file, "w") { |f| f.write '{"admins": []}' }
    end

    def status
        if !@admin_list.empty?
            @admin_list.each {|admin| puts "id: #{admin["id"]} name: #{admin["name"]}"}
        else
            puts "No admin account is found"
        end
    end

    def help
        puts "add_admin <admin_name> <password>"
        puts "delete_admin <id>"
        puts "reset_pwd <id> <new_password>"
        puts "get_id_by_name <admin_name>"
        puts "reset_all_admin"
        puts "status"
    end
end

db = ServerAdminAccess.load_admin_list 'admin_list.json'
methods = ServerAdminAccess.instance_methods
target_method = methods.detect {|m| m.to_s == ARGV[0]}
if target_method
    number_of_param = db.method(target_method).arity
    parameters = Array.new(number_of_param) {|i| "\"#{ARGV[i + 1]}\""}
    parameters = parameters.join(',')
    func = "db.#{target_method.to_s}(#{parameters})"
    eval func
    puts "sucessful execution of the method \"#{func}\""
else
    puts "method not found"
end