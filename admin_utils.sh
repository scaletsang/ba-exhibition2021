#!/bin/bash

function admin() {
  ruby admin_utils.rb $@
}

function production() {
  rake production $1
}

function development() {
  rackup -p $1
}