require 'bcrypt'
require 'json'
require_relative 'utils.rb'

class AdminDataBase
    attr_accessor :file, :admin_list
  
    def initialize file, hash
      @file = file
      @admin_list = hash['admins']
    end
  
    # Load admin list from json
    def self.load_admin_list file
      if (File.exist? file) then
        admin_list_json = ''
        File.open(file) { |f| admin_list_json = f.read }
        return AdminDataBase.new file, JSON.parse(admin_list_json)
      end
    end
    
    def refresh
      admin_list_json = ''
      File.open(@file) { |f| admin_list_json = f.read }
      admin_list = JSON.parse admin_list_json
      @admin_list = admin_list['admins']
    end
  
    def hash_password password
      BCrypt::Password.create(password).to_s
    end
  
    def test_password password, hash
      BCrypt::Password.new(hash) == password
    end
  end