require 'sinatra'
require 'sinatra-websocket'
require 'sinatra/config_file'
require 'sinatra/asset_pipeline'
require 'sinatra/sprockets-helpers'
require_relative 'world.rb'
require_relative 'client.rb'
require_relative 'map_object.rb'
require_relative 'utils.rb'
require_relative 'admin_db.rb'

class App < Sinatra::Base
  enable :sessions
  
  register Sinatra::ConfigFile
  config_file '../config.yml'
  register Sinatra::AssetPipeline
  register Sinatra::Sprockets::Helpers
  puts settings.public_folder

  # Load world from json
  if (File.exist? 'world.json') then
    world_json = ''
    File.open('world.json', 'r:UTF-8') { |f| world_json = f.read }
    set :world, World.load_world(world_json)
  else
    # Init the world
    set :world, World.new
  end

  # Set world auto save every 10 minutes
  settings.world.auto_save_world 'world.json'

  # Init database that stores admin names and corresponding passwords
  set :db, AdminDataBase.load_admin_list('admin_list.json')

  # URL route for the index page
  get '/' do

    # Serve the frontend HTML document for regular GET requests
    if !request.websocket?
			File.read "#{settings.assets_public_path}/pages/index.html"
    else # Handler for websocket connections

      request.websocket do |ws|
        # Inits a new client
        client = Client.new ws, settings.world

        # Adds the new client to the world
				ws.onopen do
          settings.world.add_client client
        end

        # Handle a message from the client
        ws.onmessage do |msg|
          begin
            client.handle_msg msg
					rescue StandardError => e
						puts "CAUGHT ERROR:\n#{e.message}#{e.backtrace.inspect}"
					end
        end

        # Removes the client from the world and closes it
        ws.onclose do
          settings.world.remove_client client
          client.close
        end
      end
    end
  end

  get '/admin' do
    #For first ever visiting the admin page
    if !session || !session[:admin_name] || !session[:admin_password]
      return File.read "#{settings.assets_public_path}/pages/admin-login.html"
    end

    case session[:login]
    when 'try', 'success'
      login_result = login session[:admin_name], session[:admin_password]
      session = login_result
    when 'logged', 'failure'
      return File.read "#{settings.assets_public_path}/pages/admin-login.html"
    else
      return "login type error"
    end

    #after receiving the login result
    if session[:login] == 'success'
      # Serve the frontend HTML document for regular GET requests
      if !request.websocket?
        File.read "#{settings.assets_public_path}/pages/cms.html"
      else # Handler for websocket connections
        request.websocket do |ws|
          # Inits a new client
          client = Client.new ws, settings.world, session[:admin_id], session[:admin_name]

          # Adds the new client to the world
          ws.onopen do
            settings.world.add_client client
          end
          
          # Handle a message from the client
          ws.onmessage do |msg|
            begin
              client.handle_msg msg
            rescue StandardError => e
              puts "CAUGHT ERROR:\n#{e.message}#{e.backtrace.inspect}"
            end
          end
          
          # Removes the client from the world and closes it
          ws.onclose do
            settings.world.remove_client client
            client.close
          end
        end
      end
    elsif session[:login] == 'logged'
      redirect '/admin#logged'
    else
      session[:login] = 'failure'
      redirect '/admin#wrong'
    end
  end

  def login username, pwd
    #read from the adminlogin list
    settings.db.refresh

    admin = settings.db.admin_list.find { |u| u['name'] == username && settings.db.test_password(pwd, u['password_hash'])}

    if admin.nil?
      session[:admin_name] = username
      session[:admin_password] = pwd
      session[:login] = 'failure'
      return session
    end

    if settings.world.client_exist? admin['id']
      session[:admin_name] = username
      session[:admin_password] = pwd
      session[:login] = 'logged'
      return session
    end

    session[:admin_id] = admin['id']
    session[:admin_name] = username
    session[:admin_password] = pwd
    session[:login] = 'success'
    return session
  end

  post '/admin' do
    session.clear
    session[:admin_name] = params[:username]
    session[:admin_password] = params[:password]
    session[:login] = 'try'
    redirect '/admin'
  end

  get '/json' do
    content_type 'application/json'
    return settings.world.map_obj_json
  end
  
  get '/invitation' do
    File.read "#{settings.assets_public_path}/pages/invitation.html"
  end

  at_exit do
    settings.world.save_world 'world.json'
    puts "Server terminated at #{Time.now}. Sucessfully saved world with the lastest edits."
  end
end
