require 'json'
require_relative 'utils.rb'
require_relative 'message_object.rb'

# A client with a websocket connection and client data
class Client

  attr_accessor :uid, :name, :pos

  def initialize ws, world, uid=generate_uid, name='new-visitor'
    @ws = ws
    @world = world
    @uid = uid
    @name = name
    @pos = { x: 0, y: 0 }
  end

  def handle_msg msg
    #puts "Client [#{@uid}] got: #{msg}"

    msg_hash = JSON.parse msg

    # handle msg according to msg's type
    case msg_hash['type']
    when 'clientInit'
      msg_hash['uid'] = @uid
      msg_hash['name'] = @name
      @pos['x'] = msg_hash['destX']
      @pos['y'] = msg_hash['destY']
      self.send_msg @world.get_all_map_objs @uid #send to myself all the map items and clients avator
      @world.add_waiting_msg msg_hash
    when 'clientMove'
      msg_hash['uid'] = @uid
      @pos['x'] = msg_hash['destX']
      @pos['y'] = msg_hash['destY']
      @world.add_waiting_msg msg_hash
    when 'nameChange'
      msg_hash['uid'] = @uid
      @name = msg_hash['name']
      @world.add_waiting_msg msg_hash
    when 'addMapObject'
      msg_hash['obj']['id'] = generate_uid
      msg_hash['obj']['author_id'] = @uid
      # upload media if object type is image or video
      media_type = msg_hash['obj']['type']
      if media_type == 'image' || media_type == 'video'
        msg_hash['obj']['dir'] = @world.media_dir
        @world.upload_media msg_hash['obj']['fileName'], media_type, msg_hash['fileData']
      end
      msg_hash.delete('fileData')
      @world.add_map_obj MapObject.from_hash msg_hash['obj']
      @world.add_waiting_msg msg_hash
    when 'updateMapObject'
      @world.update_map_obj msg_hash['id'], msg_hash['attr'], msg_hash['newValue']
      @world.add_waiting_msg msg_hash
    when 'deleteMapObject'
      try_removed_obj = @world.remove_map_obj(msg_hash['id'])
      if try_removed_obj then
        removed_obj = try_removed_obj.to_hash
        @world.delete_media removed_obj['file_name'], removed_obj['type'] if removed_obj['type'] == 'image' || removed_obj['type'] == 'video' 
        @world.add_waiting_msg msg_hash
      end
    end
  end

  def send_msg msg
    @ws.send msg
    #puts "Client [#{@uid}] sent: #{msg}"
  end

  def close
    @world.add_waiting_msg ClientCloseMsg.new(@uid).to_hash
    puts "Closed client [#{@uid}]"
  end

end
