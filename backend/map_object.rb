require_relative 'utils.rb'

# Map object prototypes
class MapObject
    attr_accessor :id, :author_id, :type, :pos, :size
    def initialize id, author_id, pos_x, pos_y, size_w, size_h
        @id = id
        @author_id = author_id
        @pos = { x: pos_x, y: pos_y }
        @size = { w: size_w, h: size_h}
    end

    def to_hash
        self.instance_variables.inject({}) {|hash, var| 
            hash[var.to_s.delete('@')] = self.instance_variable_get var
            hash
        }
    end

    def self.from_hash h
        case h['type']
        when 'image'
            return Image.new h['id'], h['author_id'], h['pos']['x'], h['pos']['y'], h['size']['w'], h['size']['h'], h['file_name'], h['dir'], h['caption']
        when 'video'
            return Video.new h['id'], h['author_id'], h['pos']['x'], h['pos']['y'], h['size']['w'], h['size']['h'], h['file_name'], h['dir']
        when 'paragraph'
            return Paragraph.new h['id'], h['author_id'], h['pos']['x'], h['pos']['y'], h['size']['w'], h['size']['h'], h['styled_text']
        when 'html'
            return Html.new h['id'], h['author_id'], h['pos']['x'], h['pos']['y'], h['size']['w'], h['size']['h'], h['html_tag'], h['html']
        when 'rect'
            return Rect.new h['id'], h['author_id'], h['pos']['x'], h['pos']['y'], h['size']['w'], h['size']['h'], h['color']
        end
    end

    def update_attr attr, new_value
        attr_underscored = underscore attr
        self.send("#{attr_underscored}=", new_value)
    end
end

class Image < MapObject
    attr_accessor :file_name
    def initialize id, author_id, pos_x, pos_y, size_w, size_h, file_name, dir, caption=''
        super id, author_id, pos_x, pos_y, size_w, size_h
        @type = 'image'
        @file_name = file_name
        @dir = dir
        @caption = caption
    end
end

class Video < MapObject
    attr_accessor :file_name
    def initialize id, author_id, pos_x, pos_y, size_w, size_h, file_name, dir
        super id, author_id, pos_x, pos_y, size_w, size_h
        @type = 'video'
        @file_name = file_name
        @dir = dir
    end
end

class Paragraph < MapObject
    attr_accessor :styled_text
    def initialize id, author_id, pos_x, pos_y, size_w, size_h, styled_text
        super id, author_id, pos_x, pos_y, size_w, size_h
        @type = 'paragraph'
        @styled_text = styled_text
    end
end

class Html < MapObject
    attr_accessor :html
    def initialize id, author_id, pos_x, pos_y, size_w, size_h, html_tag, html
        super id, author_id, pos_x, pos_y, size_w, size_h
        @type = 'html'
        @html_tag = html_tag
        @html = html
    end
end

class Rect < MapObject
    attr_accessor :color
    def initialize id, author_id, pos_x, pos_y, size_w, size_h, color
        super id, author_id, pos_x, pos_y, size_w, size_h
        @type = 'rect'
        @color = color
    end
end