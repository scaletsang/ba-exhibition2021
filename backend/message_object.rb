# Message prototypes
class Message
    attr_reader :type
    def to_hash
        self.instance_variables.inject({}) {|hash, var| 
            hash[var.to_s.delete('@')] = self.instance_variable_get var
            hash
        }
    end
end

class MapChangeMsg < Message
    attr_reader :map
    def initialize map
        @type = 'mapChange'
        @map = map
    end
end

class MapInitMsg < Message
    attr_reader :map
    def initialize map, uid
        @type = 'mapInit'
        @map = map
        @uid = uid
    end
end

class ClientCloseMsg < Message
    attr_reader :uid
    def initialize uid
        @type = 'clientClose'
        @uid = uid
    end
end