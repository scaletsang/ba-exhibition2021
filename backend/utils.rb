require 'concurrent'
require 'thread'

def generate_uid len=32
  alphabet = ['a'..'z', 'A'..'Z', '0'..'9'].map(&:to_a).join.chars
  len.times.collect{ alphabet.sample }.join
end

def schedule_task sec, func
  timer = Concurrent::ScheduledTask.new(sec) { func.call }
  timer.execute
end

def schedule_recurring_task interval, func
  timer = Concurrent::TimerTask.new(execution_interval: interval, timeout_interval: 1) { |task| func.call}
  timer.execute
end

def underscore(camel_cased_word)
  camel_cased_word.to_s.gsub(/::/, '/').
    gsub(/([A-Z]+)([A-Z][a-z])/,'\1_\2').
    gsub(/([a-z\d])([A-Z])/,'\1_\2').
    tr("-", "_").
    downcase
end