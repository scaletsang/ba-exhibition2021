require 'json'
require 'date'
require_relative 'utils.rb'
require_relative 'map_object.rb'
require_relative 'message_object.rb'

# A world containing clients and map objects
class World
  attr_reader :media_dir

  def initialize
    @clients = {}
    @map_objs = {}
    @waiting_messages = []
    @media_dir = 'media/'
  end

  # --------Methods for clients in the world--------

  def add_client c
    @clients[c.uid] = c
  end

  def remove_client c
    @clients.delete(c.uid)
  end

  def client_exist? uid
    @clients.key?(uid)
  end

  # --------Methods for Map Objects--------

  def add_map_obj obj
    @map_objs[obj.id] = obj
  end
  
  def update_map_obj id, property, new_value
    puts @map_objs[id].inspect
    @map_objs[id].update_attr property, new_value
  end

  def remove_map_obj id
    @map_objs.delete(id)
  end

  def map_obj_json
    world = @map_objs.collect { |id,obj| [id, obj.to_hash] }.to_h
    JSON.generate world
  end

  # --Methods for media management-

  def upload_media file_name, type, file_data
    file_type = 'img/' if type == 'img'
    file_type = 'video/' if type == 'video'
    path_to_file = @media_dir + file_type + file_name
    File.open(path_to_file, 'wb') { |f|
    f.write file_data.read }
  end

  def delete_media file_name, type
    file_type = 'img/' if type == 'img'
    file_type = 'video/' if type == 'video'
    path_to_file = @media_dir + file_type + file_name
    File.delete(path_to_file) if File.exist?(path_to_file)
  end

  # --Methods for saving & loading the world-

  def self.load_world file
    hash = JSON.parse file
    world = World.new
    hash.each {|id, obj| world.add_map_obj MapObject.from_hash obj}
    world
  end

  def save_world file
    File.open(file, 'w') { |f| f.write map_obj_json }
  end

  def auto_save_world file
    schedule_recurring_task 600, Proc.new {
      backup_world 'backup'
      save_world file
      puts "auto saved world at #{Time.now}"
    }
  end

  def backup_world path
    Dir.mkdir path unless Dir.exist? path
    world = @map_objs.collect { |id,obj| [id, obj.to_hash] }.to_h
    world_json = JSON.generate world
    file_name = Time.now.strftime("world_backup_on_%d-%m-%Y_at_time_%H-%M")
    file_path = "#{path}/#{file_name}"
    File.open(file_path, 'w') { |f| f.write world_json } 
  end

  # --------Methods for sending messages from the world--------

  def add_waiting_msg msg
    if @waiting_messages.empty?
      # Schedule broadcast waiting messages to all clients after 0.02 seconds
      schedule_task 0.02, Proc.new { broadcast_msg pack_msgs }
    end
    @waiting_messages << msg
  end

  def pack_msgs
    msg = MapChangeMsg.new @waiting_messages
    @waiting_messages = []
    JSON.generate msg.to_hash
  end

  def broadcast_msg msg
    @clients.values.each {|c| c.send_msg msg}
  end

  Visitor = Struct.new(:uid, :destX, :destY, :name, :type) do
    def initialize(*)
        super
        self.type = 'visitor'
    end
  end

  def get_all_map_objs uid
    # Parse each clients into an visitor map object, and put them in a list
    visitors = @clients.collect do |k, c|
      visitor = Visitor.new(c.uid, c.pos['x'], c.pos['y'], c.name)
      visitor.to_h
    end
    mapitems = @map_objs.collect { |k, c| c.to_hash }
    msg = MapInitMsg.new(mapitems + visitors, uid)
    JSON.generate msg.to_hash
  end

end
