function Visitor(posX, posY, uid='', name='new-visitor') {
  this.type = 'visitor';
  this.uid = uid;
  this.name = name;
  this.pos = new p5.Vector(posX, posY);
  this.dir = new p5.Vector(0, 0);
  this.destination = this.pos;
  this.speed = 0;
  this.tick = () => {
    if (this.pos.dist(this.destination) > 1) {
      this.pos.add(p5.Vector.mult(this.dir, this.speed));
      this.dir = p5.Vector.sub(this.destination, this.pos).normalize();
      this.speed = this.pos.dist(this.destination) / 20;
    }
  };
}

function Client() {
  //Setting constuctors from the super class Visitor, specifying the pos value.
  Visitor.call(this, 0, 0);
  this.mapDomItems = new Map();
  this.mapCanvasItems = new Map();
  this.wsConn = new WSConn((conn, rawMsg) => {
    //received message handler
    console.log(rawMsg);
    let rawMsgObj = JSON.parse(rawMsg);
    let msgList = rawMsgObj.map;
    // If the mapChange message have uid attached, set it as the uid of this client
    if ('uid' in rawMsgObj) {this.uid = rawMsgObj.uid;}
    if (rawMsgObj.type === 'mapInit') {
      //add all map objects from the received message to their corresponding rendering stack
      for (let r = 0; r < msgList.length; r++) {
        const msgObj = msgList[r];
        if (msgObj.uid === this.uid) {this.name = msgObj.name;}
        if (msgObj.type === 'visitor' || msgObj.type === 'rect') {
          this.addCanvasItem(msgObj);
        } else {
          window.addDomItem(msgObj);
          this.addDomItem(msgObj);
        }
      }
    } else if (rawMsgObj.type === 'mapChange') {
      //looping the list of messages (msgList) over the list of existing allMapitems
      for (let r =  msgList.length - 1; r >= 0; r--) {
        const msgObj = msgList[r];
        const matchItem = (list, id, func) => {
          //update position of the client when found
          if (list.has(id)) {func(list.get(id));}
        }
        //switch statement on message type
        switch(msgObj.type) {
          case 'clientInit':
            //Don't add myself
            if (msgObj.uid === this.uid) {
              if (whoAmI === 'client') {
                window.setAllButtonsEvent();
              }else if (whoAmI === 'admin') {
                window.setAllButtonsEvent_Admin();
                window.loadProfileInfo();
              }
              break;
            }
            //add to mapCanvas items
            msgObj.type = 'visitor';
            this.addCanvasItem(msgObj);
            break;
          case 'clientMove':
            //loop through mapCanvas items
            //update position of the client when found
            matchItem(this.mapCanvasItems, msgObj.uid, (canvasItem) => {
              this.updateMapItem(canvasItem, 'destination', new p5.Vector(msgObj.destX, msgObj.destY));
            });
            break;
          case 'nameChange':
            //loop through mapCanvas items
            //update name of the client when found
            matchItem(this.mapCanvasItems, msgObj.uid, (canvasItem) => {
              this.updateMapItem(canvasItem, 'name', msgObj.name);
            });
            break;
          case 'clientClose':
            //loop through mapCanvas items
            //delete client from mapcanvasitem list when found
            matchItem(this.mapCanvasItems, msgObj.uid, (canvasItem) => {
              this.mapCanvasItems.delete(msgObj.uid);
            });
            break;

          // Html element rendering
          case 'addMapObject':
            //render map object and record it to mapDom items list
            window.addDomItem(msgObj.obj);
            this.addDomItem(msgObj.obj);
            break;
          case 'updateMapObject':
            //loop through mapDom items
            //update map object attributes when found
            matchItem(this.mapDomItems, msgObj.id, (domItem) => {
              window.updateDomItem(msgObj.id, msgObj.attr, msgObj.newValue);
              this.updateMapItem(domItem, msgObj.attr, msgObj.newValue);
            });
            break;
          case 'deleteMapObject':
            //add to mapDom items temp
            window.removeDomItem(msgObj.id);
            //loop through mapDom items
            //delete map object when found
            matchItem(this.mapDomItems, msgObj.id, (domItem) => {
              this.mapDomItems.delete(msgObj.id);
            });
            break;

          // Canvas object rendering (add later)
          case 'addCanvasObject':
            //add to mapCanvas items
            break;
          case 'updateCanvasObject':
            //loop through mapCanvas items
            break;
          case 'deleteCanvasObject':
            //loop through mapCanvas items
            break;
          
          default:
            console.log(`Invalid Message type: ${msgObj.type}`);
        }
      }
    }
    console.log(...this.mapCanvasItems);
    console.log(...this.mapDomItems);
  });
  this.updateMapItem = (mapItem, property, value) => mapItem[property] = value;
  this.addCanvasItem = (msgObj) => {
    switch(msgObj.type) {
      case 'visitor':
        //load visitor avator from the world
        this.mapCanvasItems.set(msgObj.uid, new Visitor(msgObj.destX, msgObj.destY, msgObj.uid, msgObj.name));
        break;
      //Canvas objects
      case 'rect':
        this.mapCanvasItems.set(msgObj.id, new Rect(msgObj.id, msgObj.author_id, msgObj.pos.x, msgObj.pos.y, msgObj.size.w, msgObj.size.h, msgObj.color));
        break;
    }
  };
  this.addDomItem = (msgObj) => {
    switch(msgObj.type) {
      //Html elements
      case 'image':
        this.mapDomItems.set( msgObj.id, new Image(msgObj.id, msgObj.author_id, msgObj.pos.x, msgObj.pos.y, msgObj.size.w, msgObj.size.h, msgObj.fileName, msgObj.dir, msgObj.caption));
        break;
      case 'video':
        this.mapDomItems.set( msgObj.id, new Video(msgObj.id, msgObj.author_id, msgObj.pos.x, msgObj.pos.y, msgObj.size.w, msgObj.size.h, msgObj.fileName, msgObj.dir));
        break;
      case 'paragraph':
        this.mapDomItems.set( msgObj.id, new Paragraph(msgObj.id, msgObj.author_id, msgObj.pos.x, msgObj.pos.y, msgObj.size.w, msgObj.size.h, msgObj.styledText));
        break;
      case 'plain-html':
        this.mapDomItems.set( msgObj.id, new Html(msgObj.id, msgObj.author_id, msgObj.pos.x, msgObj.pos.y, msgObj.size.w, msgObj.size.h, msgObj.html_tag, msgObj.html));
        break;
      default:
        console.log(`Invalid Object type: ${msgObj.type}`);
    }
  }
  this.sendInitMsg = () => {
    let msg = new ClientInitMsg(this.destination.x, this.destination.y, this.name);
    this.wsConn.send(JSON.stringify(msg));
  };
  this.sendMovementMsg = () => {
    let msg = new ClientMoveMsg(this.destination.x, this.destination.y);
    this.wsConn.send(JSON.stringify(msg));
  };
  this.sendNameChangeMsg = () => {
    let msg = new NameChangeMsg(this.name);
    this.wsConn.send(JSON.stringify(msg));
  };
}