function Admin(ui) {
  Client.call(this);
  this.selection = {};
  this.textEditor = {};
  this.sendAddMapObjectMsg = (obj) => {
    let msg = new AddMapObjectMsg(obj);
    this.wsConn.send(JSON.stringify(msg));
  };
  this.sendUpdateMapObjectMsg = (id, attr, newValue) => {
    let msg = new UpdateMapObjectMsg(id, attr, newValue);
    this.wsConn.send(JSON.stringify(msg));
  };
  this.sendDeleteMapObjectMsg = (id) => {
    let msg = new DeleteMapObjectMsg(id);
    this.wsConn.send(JSON.stringify(msg));
  }; 
  // Add later
  // this.sendUpdateCanvasObjectMsg = () => {
  //   let msg = new UpdateCanvasObjectMsg();
  //   this.wsConn.send(JSON.stringify(msg));
  // };
  // this.sendDeleteCanvasObjectMsg = () => {
  //   let msg = new DeleteCanvasObjectMsg();
  //   this.wsConn.send(JSON.stringify(msg));
  // };
  // this.sendAddCanvasObjectMsg = () => {
  //   let msg = new AddCanvasObjectMsg();
  //   this.wsConn.send(JSON.stringify(msg));
  // };
}

function Selection(div, x, y) {
  this.div = div;
  this.startPos = {x: x, y: y};
}

function cmsUI(p) {
  UI.call(this, p);
  //---------html ui----------------
  this.setAllButtonsEvent_Admin = () => {
    this.setAllButtonsEvent();
    //admin profile container
    p.select('#admin-profile').mouseClicked( e => this.openWindow('#admin-profile-details', 'flex'));
    document.getElementById('admin-profile').addEventListener('mouseenter', e => this.openWindow('#admin-profile-details', 'flex'));
    document.getElementById('admin-profile').addEventListener('mouseleave', e => this.closeWindow(e, 'admin-profile-details'));
    //other buttons at basic ui container
    p.select('#cms-tools-button').mouseClicked( e => {
      let div = p.select('#cms-tools-container');
      div.elt.children[0].innerHTML = 'Add Content';
      div.elt.children[1].style.display = 'none';
      if (div.elt.style.display !== 'flex') {this.openWindow('#cms-tools-container', 'flex');}
      this.openWindow('#add-buttons-container', 'flex');
    });
    p.select('#add-image-button').mouseClicked(uploadMediaHandler);
    p.select('#add-paragraph-button').mouseClicked(e => {
      let selectionLayer = p.createDiv();
      selectionLayer.id('selection-layer');
      selectionLayer.class('popup');
      selectionLayer.parent(p.select('body'));
      this.openWindow(selectionLayer);
      this.makeSelection(selectionLayer, () => {
        let selectionDiv = this.client.selection.div;
        console.log(selectionDiv);
        let v = this.getAbsPos(new p5.Vector(selectionDiv.x, selectionDiv.y))
        let p = new Paragraph('', '', v.x, v.y, selectionDiv.width, selectionDiv.height, "");
        this.client.sendAddMapObjectMsg(p);
        // let textEditor = p.select('#unregistered-paragraph');
        // textEditor.position(selectionDiv.pos.x, selectionDiv.pos.y);
        // textEditor.size(selectionDiv.size.width, selectionDiv.size.height);
        // this.openWindow(textEditor);
        // this.loadTextEditor();
      });
    });
    p.select('#add-video-button').mouseClicked(uploadMediaHandler);
  }

  this.attachImageTools = (img, container) => {

  }

  this.attachVideoTools = (vid, container) => {
    
  }

  this.attachParagraphTools = (para, container) => {
    //When paragraph is double-clicked, load text editor
    para.doubleClicked( e => {
      container.removeClass('ui-frame');
      para.class('ui-frame');
      console.log(container);
      this.showTextEditor(para.id(), para.html() ,container.x, container.y, container.width, container.height);
    });

    //When paragraph is clicked, enable moving or resizing
    // para.mouseClicked( e => {
    //   this.setEventTriggerLayerAction( e => {
    //     container.removeAttribute('moving');
    //   });
    //   container.attribute('moving', true);
    // })
  }

  this.setEventTriggerLayerAction = (func) => {
    let eventTriggerLayer = p.select('#event-trigger-layer');
    eventTriggerLayer.style('display', 'block');
    let triggerEvent = (e) => {
      func(e);
      eventTriggerLayer.style('display', 'none');
    }
    eventTriggerLayer.elt.addEventListener('click', triggerEvent, { once: true});
  }

  this.loadProfileInfo = () => {
    //load random profile pic
    let randomNum = Math.floor(Math.random() * 16);
    let path = `/img/admin-profile-pic-${randomNum + 1}.png`
    p.select('#admin-profile-pic').elt.src = path;
    //load display name
    p.select('#display-name').html(this.client.name);
    p.select('#client-id').html(this.client.uid);
  }

  this.loadQuillTextEditor = () => {
    //Variable to change
    const colorPalete = ['#fd5506', '#fdb406', '#5a68f7', '#09133a', '#96a5dd', '#2b3f8a', '#ffffff'];
    const fontList = ['times-new-roman', 'georgia'];
    const fontSizeList = ['8px','9px','10px','12px','14px','16px','20px','24px','32px','42px','54px','68px','84px','98px'];
    // Specify Quill fonts
    let fonts = Quill.import('attributors/style/font');
    let size = Quill.import('attributors/style/size');
    size.whitelist = fontSizeList;
    fonts.whitelist = fontList;
    
    Quill.register(fonts, true);
    Quill.register(size, true);

    let toolbarOptions = [
      /* text style*/[{ 'header': [false, 1, 2] }],      
      /* text size*/[{ 'size': size.whitelist }],  
      /* text font*/[{ 'font': fonts.whitelist }],
  
      /* Bold, Italic, underline, strike*/['bold', 'italic', 'underline', 'strike'],
      /* text color & highlights*/[{ 'color': colorPalete }, { 'background': colorPalete }, 'blockquote', 'code-block'],
  
      /* Link*/['link'],
      /* Align*/[{ 'align': [] }],
      /* Text direction*/[{ 'direction': 'rtl' }],
      /* Numbered & Bullet list*/[{ 'list': 'ordered'}, { 'list': 'bullet' }],
  
      /* superscript/subscript*/[{ 'script': 'sub'}, { 'script': 'super' }], 
      /* Decrease/Increase indent*/[{ 'indent': '-1'}, { 'indent': '+1' }],
      /* Clear formatting*/['clean']
    ];
    //Render the text editor in the container
    let quill = new Quill('#text-editor', {
      modules: {
        syntax: true,
        toolbar: toolbarOptions
      },
      placeholder: 'Throw your words into the pond...',
      theme: 'snow'
    });
    return quill;
  }

  this.showTextEditor = (divId, divText, x, y, w, h) => {
    let quill = this.client.textEditor;
    let textEditorContainer = p.select('#text-editor-container');
    let relPos = this.getRelPos(new p5.Vector(x,y));
    //insert text in the div to the paragraph
    quill.root.innerHTML = divText;
    //set text editor size and position
    textEditorContainer.position(relPos.x, relPos.y);
    textEditorContainer.size(w,h);
    //show text editor
    textEditorContainer.style('display', 'block');
    console.log(textEditorContainer);
    //quit the texteditor when anywhere else other than the text editor is clicked 
    //(clicking on ui wouldnt activate this event either)
    let initEditorText = quill.root.innerHTML;
    this.setEventTriggerLayerAction( e => {
      let editorText = quill.root.innerHTML;
      p.select('#delete-text-button').elt.removeEventListener('click', deleteTextEvent); 
      this.hideTextEditor();
      if (editorText === initEditorText) {return;} //don't send if the text hasn't changed for a bit
      this.client.sendUpdateMapObjectMsg(divId, 'styledText', editorText);
    });

    //delete button event listener
    let deleteTextEvent = (e) => {
      p.select('#event-trigger-layer').style('display', 'none');
      textEditorContainer.style('display', 'none');
      this.client.sendDeleteMapObjectMsg(divId);
      quill.deleteText(0,quill.getLength());
    };
    p.select('#delete-text-button').elt.addEventListener('click', deleteTextEvent, { once: true});
  
  }

  this.hideTextEditor = () => {
    let quill = this.client.textEditor;
    p.select('#text-editor-container').style('display', 'none'); //hide text editor
    quill.deleteText(0,quill.getLength()) //Clear text editor text
  }

  this.makeSelection = (div, func, mode='normal', imgData='') => {
    div.mousePressed(e => this.startSelection(div, mode, imgData));
    div.mouseMoved(e => this.drawSelection(mode));
    div.mouseReleased(e => this.endSelection(div, mode, func));
  }

  this.startSelection = (parentDiv, mode='normal', imgData='') => {
    if (mode === 'normal') {
      this.client.selection = new Selection(p.createDiv(), p.mouseX, p.mouseY);
      this.client.selection.div.style('border', '3px #5a68f7 solid');
      this.client.selection.div.style('background-color', 'gray');
      this.client.selection.div.parent(parentDiv);
    } else if (mode === 'img') {
      this.client.selection = new Selection(p.createImg(imgData, 'image'), p.mouseX, p.mouseY);
      this.client.selection.div.elt.style('box-sizing', 'border-box');
    }
    this.client.selection.div.position(p.mouseX, p.mouseY);
    this.client.selection.div.style('pointer-events', 'none');
  }

  this.drawSelection = (mode='normal') => {
    if (!p.mouseIsPressed) {return;}
    let posX = p.mouseX, posY = p.mouseY;
    let vecX = p.mouseX - this.client.selection.startPos.x;
    let vecY = p.mouseY - this.client.selection.startPos.y;
    if (mode === 'img' && p.keyIsDown(p.SHIFT)) {
      this.client.selection.div.position(this.client.selection.startPos.x, this.client.selection.startPos.y);
      this.client.selection.div.size(p.AUTO, vecY);
      return;
    }
    if (p.keyIsDown(p.SHIFT)) {
      this.client.selection.div.position(this.client.selection.startPos.x, this.client.selection.startPos.y);
      this.client.selection.div.size(vecY, vecY);
      return;
    }
    if(vecX > 0) {posX = this.client.selection.startPos.x;}
    if(vecY > 0) {posY = this.client.selection.startPos.y;}
    this.client.selection.div.position(posX, posY);
    this.client.selection.div.size(p.abs(vecX), p.abs(vecY));
  }
  
  this.endSelection = (div, mode='normal', func) => {
    //remove selection layer
    div.remove();
    func();
    this.client.selection.div.remove();
  }

  this.uploadMediaHandler = () => {
    //TODO
  }
}