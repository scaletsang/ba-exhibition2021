//----------Message Types-----------

//clients
class ClientInitMsg {
    constructor(destX, destY, name) {
        this.type = 'clientInit';
        this.destX = destX;
        this.destY = destY;
        this.name = name;
    }
}

class ClientMoveMsg {
    constructor(destX, destY) {
        this.type = 'clientMove';
        this.destX = destX;
        this.destY = destY;
    }
}

class NameChangeMsg {
    constructor(name) {
        this.type = 'nameChange';
        this.name = name;
    }
}
//canvas objects
// function UpdateCanvasObjectMsg() {}
// function DeleteCanvasObjectMsg() {}
// function AddCanvasObjectMsg() {}

//html objects
class AddMapObjectMsg {
    constructor(obj, fileData = '') {
        this.type = 'addMapObject';
        this.obj = obj;
        this.fileData = fileData;
    }
}

class UpdateMapObjectMsg {
    constructor(id, attr, newValue) {
        this.type = 'updateMapObject';
        this.id = id;
        this.attr = attr;
        this.newValue = newValue;
    }
}

class DeleteMapObjectMsg {
    constructor(id) {
        this.type = 'deleteMapObject';
        this.id = id;
    }
}

//----------MapItems Types for rendering-----------

class MapObject {
    constructor(id, author_id, type, posX, posY, sizeW, sizeH) {
        this.id = id;
        this.author_id = author_id;
        this.type = type;
        this.pos = { x: posX, y: posY };
        this.size = { w: sizeW, h: sizeH };
    }
}

class Image {
    constructor(id, author_id, posX, posY, sizeW, sizeH, fileName, dir, caption) {
        MapObject.call(this, id, author_id, 'image', posX, posY, sizeW, sizeH);
        this.fileName = fileName;
        this.dir = dir;
        this.caption = caption;
    }
}

class Video {
    constructor(id, author_id, posX, posY, sizeW, sizeH, fileName, dir) {
        MapObject.call(this, id, author_id, 'video', posX, posY, sizeW, sizeH);
        this.fileName = fileName;
        this.dir = dir;
    }
}

class Paragraph {
    constructor(id, author_id, posX, posY, sizeW, sizeH, styledText) {
        MapObject.call(this, id, author_id, 'paragraph', posX, posY, sizeW, sizeH);
        this.styledText = styledText; //html posted by tinymce
    }
}

class Html {
    constructor(id, author_id, posX, posY, sizeW, sizeH, html_tag, html) {
        MapObject.call(this, id, author_id, 'plain-html', posX, posY, sizeW, sizeH);
        this.html_tag = html_tag;
        this.html = html;
    }
}

class Rect {
    constructor(id, author_id, posX, posY, sizeW, sizeH, color) {
        MapObject.call(this, id, author_id, 'rect', posX, posY, sizeW, sizeH);
        this.color = color;
    }
}

/*
Translation to p5 terms:
author_id = .class()
pos = .position()
size = .size(w, h)
caption = html()
styledText = html()

unchangable:
dir
fileName
html_tag
*/