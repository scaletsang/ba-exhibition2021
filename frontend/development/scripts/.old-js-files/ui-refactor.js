async function exhibitionInit(clientClass) {
	let core = await Core.init(clientClass)
	core.client.sendInitMsg()
	return core
}

class Core {
  constructor(client) {
			this.client = client
			this.renderer = new Renderer(this)
			this.ui = new UI(this)
			this.clientAvatorPos = new Vector(window.innerWidth/2, window.innerHeight/2)
	}

	static async init(clientClass) {
		let client = await clientClass.init()
		let core = new Core(client)
		client.core = core
		return core
	}

	startRender(){
    this.renderer.startTick()
  }

	getAbsPos(v) {
		return v.sub(this.clientAvatorPos).add(this.client.pos);
	}

	getRelPos(v) {
		return v.sub(this.client.pos).add(this.clientAvatorPos);
	}
}

class Renderer {
  constructor(coreCtx){
    this.noLoop = true
		this.core = coreCtx
  }
  
  startTick(func) {
    this.noLoop = false
		this.tick = () => func()
    window.requestAnimationFrame(this.#loop)
  }

	#loop() {
    if(noLoop) {return}
    this.tick()
    window.requestAnimationFrame(this.#loop)
  }

  noLoop() {this.noLoop = true}

	renderVisitors() {}
	translateMapItems() {
		let v = this.core.getRelPos(new Vector(0,0))
		$('#html-elt-container').position(v.x, v.y)
	}

	addMapItem(item) {}
	updateMapItem(id, property, value) {}
	removeMapItem(id) {}
}

class UI {
	constructor(coreCtx) {
		this.core = coreCtx
	}

	renderPlayerUI() {}
	setAllButtonEvent() {}
	openWindow(containerId, displayStyle='block') {}
	closeWindow(event, specificWindowId=null) {}
	appendEleAfter(refElement, element) {}
}

class Visitor {
	constructor(posX, posY, uid='', name='loading') {
		this.type = 'visitor';
		this.uid = uid;
		this.name = name;
		this.pos = new Vector(posX, posY);
		this.dir = new Vector(0, 0);
		this.destination = this.pos;
		this.speed = 0;
	}

	tick() {
		if (this.pos.dist(this.destination) > 1) {
      this.pos.add(Vector.mult(this.dir, this.speed));
      this.dir = Vector.sub(this.destination, this.pos).normalize();
      this.speed = this.pos.dist(this.destination) / 20;
    }
	}
}

class Client extends Visitor {
  constructor(){
		super(0,0)
    this.mapItemsMap = new Map()
  	this.visitorsMap = new Map()
  }
  
  static async init() {
		let client = new Client()
		client.wsConn = await WSConn.init(client)
		return client
  }

	/**
	 * @param {WSConn} ws
	 */
	set wsConn(ws) {
		if (ws instanceof WSConn) {this.wsConn = ws}
	}

	get msgHandler(ws, rawMsg) {
		//received message handler
    console.log(rawMsg);
    let rawMsgObj = JSON.parse(rawMsg);
    this.#msgList = rawMsgObj.map;
    // If the mapChange message have uid attached, set it as the uid of this client
    if ('uid' in rawMsgObj) {this.uid = rawMsgObj.uid;}
    if (rawMsgObj.type === 'mapInit') {
      this.#mapInit()
    } else if (rawMsgObj.type === 'mapChange') {
      //looping the list of messages (msgList) over the list of existing allMapitems
      for (let r =  this.#msgList.length - 1; r >= 0; r--) {
        const msgObj = this.#msgList[r];
        
        //switch statement on message type
        switch(msgObj.type) {
          case 'clientInit':
            this.#clientInit(msgObj)
            break
          case 'clientMove':
            this.#clientMove(msgObj)
            break
          case 'nameChange':
            this.#nameChange(msgObj)
            break
          case 'clientClose':
            this.#clientClose(msgObj)
            break

          // Html element rendering
          case 'addMapObject':
            this.#addMapObject(msgObj)
            break
          case 'updateMapObject':
            this.#updateMapObject(msgObj)
            break
          case 'deleteMapObject':
            this.#deleteMapObject(msgObj)
            break
          
          default:
            console.log(`Invalid Message type: ${msgObj.type}`)
        }
      }
    }
    console.log(...this.visitorsMap)
    console.log(...this.mapItemsMap)
	}

////////////////////////////////////////////////////////
//////       Handler of each message type        ///////
////////////////////////////////////////////////////////

	#matchItem(list, id, func) {
		//update position of the client when found
		if (list.has(id)) {func(list.get(id))}
	}

	#mapInit() {
		//add all map objects from the received message to their corresponding rendering stack
		for (let r = 0; r < this.#msgList.length; r++) {
			const msgObj = this.#msgList[r]
			if (msgObj.uid === this.uid) {this.name = msgObj.name;}
			if (msgObj.type === 'visitor' || msgObj.type === 'rect') {
				this.addVisitor(msgObj)
			} else {
				this.core.ui.addDomItem(msgObj)
				this.addDomItem(msgObj)
			}
		}
	}

	#clientInit(msgObj) {
		//Don't add myself
		if (msgObj.uid === this.uid) return
		//add to mapCanvas items
		this.addVisitor(msgObj)
	}
	
	#clientMove(msgObj) {
		//loop through the list of visitors
		//update position of the client when found
		this.#matchItem(this.visitorsMap, msgObj.uid, (visitor) => {
			this.updateMapItem(visitor, 'destination', new Vector(msgObj.destX, msgObj.destY))
		})
	}

	#nameChange(msgObj) {
		//loop through the list of visitors
		//update name of the client when found
		this.#matchItem(this.visitorsMap, msgObj.uid, (visitor) => {
			this.updateMapItem(visitor, 'name', msgObj.name)
		})
	}

	#clientClose(msgObj) {
		//loop through mapCanvas items
		//delete client from mapcanvasitem list when found
		this.#matchItem(this.visitorsMap, msgObj.uid, (visitor) => {
			this.visitorsMap.delete(msgObj.uid)
		})
	}

	#addMapObject(msgObj) {
		//render map object and record it to mapDom items list
		this.core.ui.addDomItem(msgObj.obj)
		this.addDomItem(msgObj.obj)
	}

	#updateMapObject(msgObj) {
		//loop through mapDom items
		//update map object attributes when found
		this.#matchItem(this.mapItemsMap, msgObj.id, (domItem) => {
			this.core.ui.updateDomItem(msgObj.id, msgObj.attr, msgObj.newValue)
			this.updateMapItem(domItem, msgObj.attr, msgObj.newValue)
		})
	}

	#deleteMapObject() {
		//add to mapDom items temp
		this.core.ui.removeDomItem(msgObj.id);
		//loop through mapDom items
		//delete map object when found
		this.#matchItem(this.mapItemsMap, msgObj.id, (domItem) => {
			this.mapItemsMap.delete(msgObj.id)
		});
	}

	updateMapItem(mapItem, property, value) {mapItem[property] = value}

	addVisitor(msgObj) {
		this.visitorsMap.set(msgObj.uid, new Visitor(msgObj.destX, msgObj.destY, msgObj.uid, msgObj.name));
	}

	addDomItem(msgObj) {
    switch(msgObj.type) {
      //Html elements
      case 'image':
        this.mapItemsMap.set( msgObj.id, new Image(msgObj.id, msgObj.author_id, msgObj.pos.x, msgObj.pos.y, msgObj.size.w, msgObj.size.h, msgObj.fileName, msgObj.dir, msgObj.caption))
        break
      case 'video':
        this.mapItemsMap.set( msgObj.id, new Video(msgObj.id, msgObj.author_id, msgObj.pos.x, msgObj.pos.y, msgObj.size.w, msgObj.size.h, msgObj.fileName, msgObj.dir))
        break
      case 'paragraph':
        this.mapItemsMap.set( msgObj.id, new Paragraph(msgObj.id, msgObj.author_id, msgObj.pos.x, msgObj.pos.y, msgObj.size.w, msgObj.size.h, msgObj.styledText))
        break
      case 'plain-html':
        this.mapItemsMap.set( msgObj.id, new Html(msgObj.id, msgObj.author_id, msgObj.pos.x, msgObj.pos.y, msgObj.size.w, msgObj.size.h, msgObj.html_tag, msgObj.html))
        break
      default:
        console.error(`Invalid Object type: ${msgObj.type}`)
    }
  }

	sendInitMsg() {
    let msg = new ClientInitMsg(this.destination.x, this.destination.y, this.name)
    this.wsConn.send(JSON.stringify(msg))
  }

  sendMovementMsg() {
    let msg = new ClientMoveMsg(this.destination.x, this.destination.y)
    this.wsConn.send(JSON.stringify(msg))
  }

  sendNameChangeMsg() {
    let msg = new NameChangeMsg(this.name)
    this.wsConn.send(JSON.stringify(msg))
  }
}

// A Websocket connection
class WSConn {

	constructor(ws, client) {
		this.ws = ws
		this.client = client
		this.ws.onopen = (e) => {
			console.log('Connected to server.')
		}
		this.ws.onclose = () => {
			console.log('Connection to server terminated.')
			// Init client with WS connection
		  this.reconnect().then( res => this.client.sendInitMsg())
		}
		this.ws.onmessage = (rawMsg) => {
			this.client.msgHandler(this, rawMsg.data);
		}
	}
	
	static async init(client) {
		let ws = new WebSocket('ws://' + window.location.host + window.location.pathname)
		return new WSConn(ws, client)
	}

  send(msg) {this.ws.send(msg)}
	async reconnect() {
		this.client.wsConn = await WSConn.init(this.client.msgHandler) 
	}
}


class Vector {
	constructor(x, y) {
		if (typeof x != 'number' || typeof y != 'number') {
			throw new TypeError(`Expecting 2 number type. Instead, you passed a vector of ( ${typeof x}, ${typeof y} )`)
		}
		this.x = x
		this.y = y
	}

	sub(v) {
		if (v instanceof Vector) {
			this.x -= v.x
			this.y -= v.y
			return this
		}

		if (typeof v === 'number') {
			this.x -= v
			this.y -= v
			return this
		}
	}

	add(v) {
		if (v instanceof Vector) {
			this.x += v.x
			this.y += v.y
			return this
		}

		if (typeof v === 'number') {
			this.x += v
			this.y += v
			return this
		}
	}

	mult(v) {
		if (v instanceof Vector) {
			this.x *= v.x
			this.y *= v.y
			return this
		}

		if (typeof v === 'number') {
			this.x *= v
			this.y *= v
			return this
		}
	}

	dist(v) {
		const distX = v.x - this.x
		const distY = v.y - this.y
		return Math.sqrt(Math.pow(distX, 2) + Math.pow(distY, 2))	
	}

	mag() {return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2))}

	normalize() {
		const len = this.mag()
		if (len !== 0) {this.mult(1 / len)}
		return this
	}

	static #checkType(v1, v2) {
		if (!v1 instanceof Vector) throw new TypeError('expecting first paremeter to be a Vector object') 
		if (!v2 instanceof Vector || typeof v2 !== 'number') throw new TypeError('expecting second paremeter to be either a Vector object or a number')
		return typeof v2
	}

	static sub(v1, v2) {
		if (Vector.#checkType(v1, v2) === 'number') {
			return new Vector(v1.x - v2, v1.y - v2)
		} else {
			return new Vector(v1.x - v2.x, v1.y - v2.y)
		}
	}
	static add(v1, v2) {
		if (Vector.#checkType(v1, v2) === 'number') {
			return new Vector(v1.x + v2, v1.y + v2)
		} else {
			return new Vector(v1.x + v2.x, v1.y + v2.y)
		}
	}
	static mult(v1, v2) {
		if (Vector.#checkType(v1, v2) === 'number') {
			return new Vector(v1.x * v2, v1.y * v2)
		} else {
			return new Vector(v1.x * v2.x, v1.y * v2.y)
		}
	}
	static dist(v1, v2) {
		const distX = v2.x - v1.x
		const distY = v2.y - v1.y
		return Math.sqrt(Math.pow(distX, 2) + Math.pow(distY, 2))
	}
}

let vec1 = new Vector(7,3)
let vec2 = new Vector(5,17)
const core = exhibitionInit(Client)
console.log(core.client)