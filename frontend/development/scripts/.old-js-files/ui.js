function UI(p) {
  let _red_color = '#fd5506';
  let _yellow_color = '#fdb406';
  let _blue_color = '#5a68f7';
  let _dark_blue_color = '#09133a';
  let _pastel_blue_color = '#96a5dd';
  let _dark_pastel_blue_color = '#2b3f8a';
  let playerUIpos;

  // Init client with WS connection
  if (whoAmI === 'client') {this.client = new Client();}
  else if (whoAmI === 'admin') {this.client = new Admin();}

  // Init canvas
  p.setup = () => {
    p.createCanvas(p.windowWidth, p.windowHeight);
    p.pixelDensity(1);
    //Set avators name font size
    p.textAlign(p.CENTER);
    p.textSize(20);
    if (this.client instanceof Admin) {
      this.client.textEditor = this.loadQuillTextEditor();
    }
    playerUIpos = new p5.Vector (p.windowWidth/2, p.windowHeight/2);
    setTimeout(() => {this.client.sendInitMsg()}, 1000);
  };

  // Rendered each frame
  p.draw = () => {
    // Calculating
    this.client.tick();

    // Rendering
    p.clear();
    this.renderDomItemsPos();
    this.renderCanvasItems();
    this.renderPlayerUI();
  };

  //Resize canvas when window size change
  p.windowResized = () => p.resizeCanvas(p.windowWidth, p.windowHeight);

  // Event for mouse click
  p.mouseClicked = (e) => {
    if (e.target.nodeName === 'BODY') {
      let dest = this.getAbsPos(new p5.Vector(p.mouseX, p.mouseY));
      this.client.destination = dest;
      this.client.sendMovementMsg();
    }
  };

  // Gets the position of the item on the map
  this.getAbsPos = (v) => {
    let absPos = p5.Vector.sub(v, playerUIpos);
    absPos.add(this.client.pos);
    return absPos;
  }
  // Gets the canvas position of an item relative to the player
  this.getRelPos = (v) => {
    let relPos = p5.Vector.sub(v, this.client.pos);
    relPos.add(playerUIpos);
    return relPos;
  };

  // Render player user interface
  this.renderPlayerUI = () => {
    // Render player
    p.noStroke();
    p.fill(_blue_color);
    p.ellipse(playerUIpos.x, playerUIpos.y, 10, 10);
    p.fill(_blue_color);
    p.text(this.client.name, playerUIpos.x, playerUIpos.y + 30);

    // Render destination
    var destPos = this.getRelPos(this.client.destination);
    p.fill(_dark_blue_color);
    p.ellipse(destPos.x, destPos.y, 5, 5);
  };

  this.renderCanvasItems = () => {
    if (this.client.mapCanvasItems.size === 0) {return;}
    this.client.mapCanvasItems.forEach( (item, uid) => {
      // Don't render myself
      if (uid === this.client.uid) {return;}
      let itemPos = this.getRelPos(item.pos);
      switch(item.type) {
        case 'visitor':
          item.tick();
          p.noStroke();
          p.fill(_blue_color);
          p.circle(itemPos.x, itemPos.y, 10);
          p.fill(_blue_color);
          p.text(item.name, itemPos.x, itemPos.y + 30);
          break;
        case 'rect':
          p.fill(item.color);
          p.rect(itemPos.x, itemPos.y, item.size.w, item.size.h);
          break;
      }
    });
  };

  this.renderDomItemsPos = () => {
    let htmlContainer = p.select('#html-elt-container');
    let v = this.getRelPos(new p5.Vector(0,0));
    htmlContainer.position(v.x, v.y);
  };

  //---------Non-lopp-Functions----------------

  this.addDomItem = (item) => {
    // Create div for wrapping the element
    let container = p.createDiv();
    container.class(`${item.type}-container`);
    container.addClass(item.author_id);
    container.addClass('ui-frame');
    container.parent('html-elt-container');
    container.position(item.pos.x, item.pos.y);

    // Render the Dom Items
    switch(item.type) {
      case 'image':
        // Create an img DOM element on the map
        let img = p.createImg(`${item.dir}img/${item.fileName}`, item.fileName, '', () => {
          img.size(item.size.w, item.size.h);
          img.id(item.id);
          img.class(item.author_id);
          container.child(img);
        });
        // Create a p DOM element for the img's caption
        if(item.caption.length > 0) {
          let caption = p.createP(item.caption);
          caption.id(`img_caption_${item.id}`);
          caption.class(item.author_id);
          container.child(caption);
        }
        if (this.client instanceof Admin) {this.attachImageTools(img, container);}
        break;
      case 'video':
        // Create a video DOM element on the map
        let vid = p.createVideo(`${item.dir}video/${item.fileName}`, () => {
          vid.size(item.size.w, item.size.h);
          vid.id(item.id);
          vid.class(item.author_id);
          container.child(vid);
          if (this.client instanceof Admin) {this.attachVideoTools(vid, container);}
        });
        break;
      case 'paragraph':
        // Create a p DOM element on the map
        let para = p.createDiv();
        if (item.styled_text != null) {para.elt.innerHTML = item.styled_text;}
        container.size(item.size.w, item.size.h);
        para.id(item.id);
        container.child(para);
        if (this.client instanceof Admin) {this.attachParagraphTools(para, container);}
        break;
      case 'plain-html':
        // Create an html element with a DOM tag
        let html = p.createElement(item.html_tag, item.html);
        html.size(item.size.w, item.size.h);
        html.id(item.id);
        html.class(item.author_id);
        container.remove();
        break;
    }
  };

  this.updateDomItem = (id, property, value) => {
    let item = p.select(`[id='${id}']`);
    if (item == null) {
      console.log(`Cannot find id ${id} when trying to update this map object`);
      return;
    }
    let p5Property = () => {
      switch (property) {
        case 'author_id':
          return 'class';
        case 'pos':
          return 'position';
        case 'caption':
        case 'styledText':
          return 'html';
        default:
          return property;
      }
    }
    //edge case for pos and size, bec they need two variables when calling .position(x,y) or size(x,y)
    if (property === 'pos' || property === 'size') {
      return item[p5Property()](value.x, value.y);
    }
    item[p5Property()](value);
  }

  this.removeDomItem = (id) => {
    let itemToBeDeleted = p.select(`#${id}`);
    // If item is found on map, delete that item.
    if (itemToBeDeleted != null) {itemToBeDeleted.elt.parentNode.remove();}
  };

  //---------html ui----------------
  this.setAllButtonsEvent = () => {
    //basic ui
    p.select('#help-button', '#basic-ui-container').mouseClicked( e => {
      let div = p.select('#cms-tools-container');
      div.elt.children[0].innerHTML = `Welcome! ${this.client.name}!`;
      div.elt.children[2].style.display = 'none';
      if (div.elt.style.display !== 'flex') {this.openWindow('#cms-tools-container', 'flex');}
      this.openWindow('#explanation-window-container', 'flex');
    });
    p.select('#map-button', '#basic-ui-container').mouseClicked( e => this.openWindow('#map-window-container', 'flex'));
    //map teleportation button
    p.selectAll('.teleport-icons', '#map-window-container').forEach(button => {
      button.mouseClicked(e => console.log('add later')); //To DO
    });
    //hide buttons
    p.selectAll('.hide-buttons').forEach(button => {
      button.mouseClicked(this.closeWindow);
    });
  }
  this.openWindow = (containerId, displayStyle='block') => {
    let targetContainer;
    if (typeof(containerId) === 'string') {
      targetContainer = p.select(containerId);
    } else {
      targetContainer = containerId 
    }
    targetContainer.style('display', displayStyle);
    targetContainer.style('opacity', 0);
    setTimeout(() => {targetContainer.style('opacity', 1);}, 200); 
  }
  this.closeWindow = (event, specifiedWindowId=null) => {
    let targetWindow = event.target.parentElement;
    if (specifiedWindowId) { targetWindow = document.getElementById(specifiedWindowId);} 
    targetWindow.style.opacity = 0;
    setTimeout(() => {targetWindow.style.display = 'none'}, 200);
  }
  this.appendEleAfter = (refEle, ele) => {
    refEle.parentNode.insertBefore(ele, refEle.nextSibling);
  }
}
