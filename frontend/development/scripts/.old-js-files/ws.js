
// A Websocket connection
function WSConn(msgHandler) {
  // Init
  this.ws = new WebSocket('ws://' + window.location.host + window.location.pathname);
  this.ws.onopen = (e) => {
    console.log('Connected to server.');

  };
  this.ws.onclose = () => {
    console.log('Connection to server terminated.');
     // Init client with WS connection
     setTimeout(this.reconnect(), 200);
    
    };
  this.ws.onmessage = (rawMsg) => {
    var msg = rawMsg.data;
    msgHandler(this, msg);
  }
  this.send = (msg) => this.ws.send(msg);
  this.reconnect = () => {
    if (whoAmI === 'client') {window.client = new Client();}
    else if (whoAmI === 'admin') {
      window.client = new Admin();
      window.client.textEditor = window.loadQuillTextEditor();
    }
    setTimeout(() => {window.client.sendInitMsg()}, 1000);
    console.log('Reconnected to the server');
  };
}
