// @ts-check
class AdminEventManager extends EventManager {
  constructor(coreCtx) {
    super(coreCtx)
  }

  setAllListeners() {
    super.setAllListeners()
    this.#adminProfile()
    this.#cmsToolsButton()
    this.#addImageButton()
    this.#addParagraphButton()
    this.#addVideoButton()
  }

  #adminProfile() {
    return $("#admin-profile").on("click mouseenter", (() => {
      this.openWindow("#admin-profile-details", "flex")
    })).mouseleave((e => {
      this.closeWindow(e, "#admin-profile-details")
    }))
  }

  #cmsToolsButton() {
    return $("#cms-tools-button").click((() => {}))
  }

  #addImageButton() {
    return $("#add-image-button").click((() => {}))
  }

  #addParagraphButton() {
    return $("#add-paragraph-button").click((() => {}))
  }

  #addVideoButton() {
    return $("#add-video-button").click((() => {}))
  }

  mapItemOnClick(obj) {}
  imageOnDoubleClick(obj) {}
  videoOnDoubleClick(obj) {}
  paragraphOnDoubleClick(obj) {}
}