// @ts-check
class Admin extends Client {
  constructor() {
    super()
    this.selection = {}
    this.textEditor = {}
  }

  static async init() {
    let client = new Admin
    return client.wsConn = await WSConn.init(client), client
  }

  sendAddMapObjectMsg(obj) {
    let msg = new AddMapObjectMsg(obj)
    this.wsConn.send(JSON.stringify(msg))
  }

  sendUpdateMapObjectMsg(id, attr, newValue) {
    let msg= new UpdateMapObjectMsg(id, attr, newValue)
    this.wsConn.send(JSON.stringify(msg))
  }

  sendDeleteMapObjectMsg(id) {
    let msg = new DeleteMapObjectMsg(id)
    this.wsConn.send(JSON.stringify(msg))
  }
}