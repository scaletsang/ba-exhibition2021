// @ts-check
class Selection {
  constructor(div, x, y) {
    this.div = div
    this.startPos = {x: x, y: y}
  }
  startSelection() {}
  drawSelection() {}
  endSelection() {}
}