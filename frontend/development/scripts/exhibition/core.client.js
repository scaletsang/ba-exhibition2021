/// @ts-check
import * as Message from './type.message'
import Vector from './type.vector'
import WSConn from './core.client.ws'
import TypeRegister from './core.client.type-register'

export class Visitor {
  constructor(posX, posY, uid = "", name = "loading") {
    this.type = "visitor"
    this.uid = uid
    this.name = name
    this.pos = new Vector(posX, posY)
    this.dir = new Vector(0, 0)
    this.destination = this.pos
    this.speed = 0
  }

  tick() {
    if (this.pos.dist(this.destination) > 1) {
      this.pos.add(Vector.mult(this.dir, this.speed))
      this.dir = Vector.sub(this.destination, this.pos).normalize()
      this.speed = this.pos.dist(this.destination) / 20
    }
  }
}

export default class Client extends Visitor {
  constructor(posX, posY) {
    super(posX, posY)
    this.mapItemsMap = new Map()
    this.visitorsMap = new Map()
    this.typeManager = new TypeRegister()
  }

  static async init(posX, posY) {
    let client = new Client(posX, posY)
    return await client.startWSConn()
  }

  setRenderer(renderer) {
    this.renderer = renderer
  }

  async startWSConn() {
    let ws = await WSConn.init(this)
    this.wsConn = ws
    return this 
  }

  msgHandler(rawMsg) {
    console.log(rawMsg)
    let jsonMsg = JSON.parse(rawMsg)
    if ("uid" in jsonMsg) this.uid = jsonMsg.uid
    if (jsonMsg.type === "mapInit") {
      this.mapInit(jsonMsg.map)
    } else if (jsonMsg.type === "mapChange")
      jsonMsg.map.forEach(parsedMsg => {
        this.matchItemHelper(this.typeManager.message, parsedMsg.type, (messageType) => {
          messageType.callback.call(parsedMsg, this, this.renderer)
        })
      })
    console.log(...this.visitorsMap), console.log(...this.mapItemsMap)
  }

  matchItemHelper(map, id, func) {
    map.has(id) && func(map.get(id))
  }
  mapInit(msgList) {
    for (let i = 0; i < msgList.length; i++) {
      const item = msgList[i]
      //don't render myself
      if (item.uid === this.uid) {return this.name = item.name, null}
      if (item.type === 'visitor') {return this.addVisitor(item)}
      this.renderer.addDomItem(item)
      this.addDomItem(item)
    }
  }

  updateMapItem(mapObject, attr, newValue) {
    mapObject[attr] = newValue
  }
  addVisitor(parsedMsg) {
    this.visitorsMap.set(parsedMsg.uid, new Visitor(parsedMsg.destX, parsedMsg.destY, parsedMsg.uid, parsedMsg.name))
  }
  addDomItem(parsedMsg) {
    this.matchItemHelper(this.typeManager.mapObject, parsedMsg.type, (mapObjectType) => {
      this.mapItemsMap.set(parsedMsg.id, new mapObjectType(parsedMsg))
    })
  }

  sendInitMsg() {
    let msg = new Message.ClientInitMsg(this.destination.x, this.destination.y, this.name)
    this.wsConn.send(JSON.stringify(msg))
  }
  sendMovementMsg() {
    let msg = new Message.ClientMoveMsg(this.destination.x, this.destination.y)
    this.wsConn.send(JSON.stringify(msg))
  }
  sendNameChangeMsg() {
    let msg = new Message.NameChangeMsg(this.name)
    this.wsConn.send(JSON.stringify(msg))
  }
}

