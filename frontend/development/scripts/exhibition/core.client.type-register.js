/// @ts-check
import * as MapObject from './type.map-object'
import * as Message from './type.message'

export default class TypeRegister {
  constructor(importDefault=true) {
    this.message = new Map
    this.mapObject = new Map
    if (importDefault) {
      this.register('mapObject', MapObject.Paragraph, MapObject.Video, MapObject.Img)
      this.register('message', Message.ClientInitMsg, Message.ClientMoveMsg, Message.NameChangeMsg, Message.ClientCloseMsg, Message.AddMapObjectMsg, Message.UpdateMapObjectMsg, Message.DeleteMapObjectMsg)
    }
  }

  register(type, ...subtypes) {
    if (this.hasOwnProperty(type) === false) return console.error(`Cannot find "${type}" type`)
    subtypes.forEach(subtype => {
      this[type].set(subtype.registerName, subtype)
      this[subtype.name] = subtype
    })
  }

  remove(type, ...subtypes) {
    if (this.hasOwnProperty(type) === false) return console.error(`Cannot find "${type}" type`)
    subtypes.forEach(subtype => {
      this[type].delete(subtype.registerName)
      delete this[subtype.name]
    })
  }
}