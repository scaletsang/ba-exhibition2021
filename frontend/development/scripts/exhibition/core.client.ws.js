// @ts-check
export default class WSConn {

  constructor(ws, client) {
    this.ws = ws
    this.client = client

    this.ws.onopen = () => {
      console.log("Connected to server.")
    }

    this.ws.onclose = () => {
      console.log("Connection to server terminated.")
      this.reconnect().then(() => this.client.sendInitMsg())
    }

    this.ws.onmessage = (rawMsg) => {
      this.client.msgHandler(this, rawMsg.data)
    }
  }

  static async init(client) {
    let ws = new WebSocket("ws://" + window.location.host + window.location.pathname);
    return new WSConn(ws, client)
  }

  send(msg) {
    this.ws.send(msg)
  }

  async reconnect() {
    this.client.wsConn = await WSConn.init(this.client)
  }
}
