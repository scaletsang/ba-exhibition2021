// @ts-check
export default class EventManager {
  constructor(coreCtx) {
    this.core = coreCtx
    this.buttons = new Map
  }

  setAllListeners() {
    this.#helpButton()
    this.#mapButton()
    this.#teleportButtons()
    this.#hideButtons()
  }

  // @ts-ignore
  #helpButton() {
    return $("#help-button").click((() => {}))
  }

  // @ts-ignore
  #mapButton() {
    return $("#map-button").click((() => {}))
  }

  // @ts-ignore
  #teleportButtons() {
    return $(".teleport-icons").each(((e, btn) => {
      $(btn).click((() => {}))
    }))
  }

  // @ts-ignore
  #hideButtons() {
    return $(".hide-buttons").each(((e, btn) => {
      $(btn).click(this.closeWindow)
    }))
  }
  
  htmlOnDoubleClick(e) {}
  openWindow(containerId, displayStyle='block') {}
  closeWindow(event, specificWindowId=null) {}
}