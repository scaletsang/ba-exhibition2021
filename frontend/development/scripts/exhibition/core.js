// @ts-check
import Client from './core.client'
import UIRenderer from './core.ui-renderer'
import EventManager from './core.events'

export async function exhibitionInit(clientConstructor=Client, rendererConstructer=UIRenderer, eventManagerConstructer=EventManager) {
  let exhibition = await Exhibition.init(true, clientConstructor, rendererConstructer, eventManagerConstructer)
  exhibition.client.sendInitMsg()
  return exhibition
}

export default class Exhibition {

  constructor(client, uiRenderer, eventManager) {
    this.client = client
    this.renderer = new uiRenderer(client)
    this.events = new eventManager(this)
  }

  static async init(wsConnection=true, clientConstructor=Client, rendererConstructer=UIRenderer, eventManagerConstructer=EventManager) {
    let client = wsConnection ? await clientConstructor.init(0,0) : new clientConstructor(0,0)
    let exhibition = new Exhibition(client, rendererConstructer, eventManagerConstructer)
    client.setRenderer(exhibition.renderer)
    return exhibition
  }

  startRender() {
    this.renderer.startTick()
  }

  addMapObjectType(...types) {
    this.client.typeManager.register('mapObject', ...types)
  }

  addMessageType(...types) {
    this.client.typeManager.register('message', ...types)
  }
}

