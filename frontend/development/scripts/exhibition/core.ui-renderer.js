// @ts-check
import Vector from "./type.vector"

export default class UIRenderer {
  constructor(client) {
    this.noLoop = true
    this.client = client
    this.clientAvatorPos = new Vector(window.innerWidth / 2, window.innerHeight / 2)
  }

  startTick() {
    this.noLoop = false
    this.#loop()
  }

  tick() {

  }

  // @ts-ignore
  #loop() {
    this.noLoop || (this.tick(), window.requestAnimationFrame(this.#loop))
  }

  setNoLoop() {
    this.noLoop = false
  }

  getAbsPos(v) {
    return v instanceof Vector && v.sub(this.clientAvatorPos).add(this.client.pos)
  }

  getRelPos(v) {
    return v instanceof Vector && v.sub(this.client.pos).add(this.clientAvatorPos)
  }

  renderAvatar() {}
  renderMyself() {}
  renderVisitors() {}
  translateAllMapItems() {
    let vector = this.getRelPos(new Vector(0, 0))
    $("#html-elt-container").css({top: vector.y, left: vector.x})
  }

  addMapItem(msg) {}

  updateMapItem(id, property, value) {}

  removeMapItem(id) {}

  appendEleAfter(relElement, element) {}
}