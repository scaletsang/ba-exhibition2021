// @ts-check
export class MapObjectTemplate {
  constructor({ id, author_id, type, pos, size }) {
    this.id = id
    this.author_id = author_id
    this.type = type
    this.pos = pos
    this.size = size
  }

  enableResize(eventManager) {
    eventManager.mapItemOnClick(this)
  }

  enableEdit(eventManager) {
    eventManager[`${this.type}OnDoubleClick`](this)
  }
}

export class Img extends MapObjectTemplate {
  constructor({ id, author_id, pos, size, fileName, dir, caption }) {
    super({ id, author_id, type: Img.registerName, pos, size })
    this.fileName = fileName
    this.dir = dir
    this.caption = caption
  }
  static registerName = 'image'
  
}

export class Video extends MapObjectTemplate {
  constructor({ id, author_id, pos, size, fileName, dir }) {
    super({ id, author_id, type: Video.registerName, pos, size })
    this.fileName = fileName
    this.dir = dir
  }
  static registerName = 'video'
}

export class Paragraph extends MapObjectTemplate {
  constructor({ id, author_id, pos, size, styledText }) {
    super({ id, author_id, type: Paragraph.registerName, pos, size })
    this.styledText = styledText
  }
  static registerName = 'paragraph'
}

export class Html extends MapObjectTemplate {
  constructor({ id, author_id, pos, size, html_tag, html }) {
    super({ id, author_id, type: Html.registerName, pos, size })
    this.html_tag = html_tag
    this.html = html
  }
  static registerName = 'html'
}