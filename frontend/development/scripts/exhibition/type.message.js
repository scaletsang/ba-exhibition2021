// @ts-check
import Vector from './type.vector'

export class MessageTemplate {
  constructor() {
    this.type = 'unregisteredType' 
  }
  static registerName = 'unregisteredType'
  static callback = {
    callback(client, ui) {}
  }
}

export class ClientInitMsg extends MessageTemplate {
  constructor(destX, destY, name, uid='') {
    super()
    this.type = ClientInitMsg.registerName
    this.destX = destX
    this.destY = destY
    this.name = name
    this.uid = uid
  }

  static registerName = 'clientInit'
  static callback = {
    callback(client, ui) {
      if (this.uid !== client.uid) {
        client.addVisitor(this)
        ui.renderVisitor(this)
      }
    }
  }
}

export class ClientMoveMsg extends MessageTemplate {
  constructor(destX, destY, uid='') {
    super()
    this.type = ClientMoveMsg.registerName
    this.destX = destX
    this.destY = destY
    this.uid = uid
  }

  static registerName = 'clientMove'
  static callback = {
    callback(client, ui) {
      client.matchItemHelper(client.visitorsMap, this.uid, (visitor => {
        client.updateMapItem(visitor, "destination", new Vector(this.destX, this.destY))
      }))
    }
  }
}

export class NameChangeMsg extends MessageTemplate {
  constructor(name, uid='') {
    super()
    this.type = NameChangeMsg.registerName
    this.name = name
    this.uid = uid
  }

  static registerName = 'nameChange'
  static callback = {
    callback(client, ui) {
      client.matchItemHelper(client.visitorsMap, this.uid, (visitor => {
        client.updateMapItem(visitor, "name", this.name)
        //TODO renderer namechange
      }))
    }
  }
}

export class ClientCloseMsg extends MessageTemplate {
  constructor(uid) {
    super()
    this.type = ClientCloseMsg.registerName
    this.uid = uid
  }

  static registerName = 'clientClose'
  static callback = {
    callback(client, ui) {
      client.matchItemHelper(client.visitorsMap, this.uid, ((_) => {
        client.visitorsMap.delete(this.uid)
        //TODO renderer delete from map
      }))
    }
  }
}

export class AddMapObjectMsg extends MessageTemplate {
  constructor(obj, fileData = '') {
    super()
    this.type = AddMapObjectMsg.registerName
    this.obj = obj
    this.fileData = fileData
  }

  static registerName = 'addMapObject'
  static callback = {
    callback(client, ui) {
      client.addDomItem(this.obj)
      ui.addDomItem(this.obj)
    }
  }
}

export class UpdateMapObjectMsg extends MessageTemplate {
  constructor(id, attr, newValue) {
    super()
    this.type = UpdateMapObjectMsg.registerName
    this.id = id
    this.attr = attr
    this.newValue = newValue
  }

  static registerName = 'updateMapObject'
  static callback = {
    callback(client, ui) {
      client.matchItemHelper(client.mapItemsMap, this.id, (mapObject => {
        ui.updateDomItem(this.id, this.attr, this.newValue)
        client.updateMapItem(mapObject, this.attr, this.newValue)
      }))
    }
  }
}

export class DeleteMapObjectMsg extends MessageTemplate {
  constructor(id) {
    super()
    this.type = DeleteMapObjectMsg.registerName
    this.id = id
  }

  static registerName = 'deleteMapObject'
  static callback = {
    callback(client, ui) {
      ui.removeDomItem(this.id)
      client.matchItemHelper(client.mapItemsMap, this.id, (() => {
        client.mapItemsMap.delete(this.id)
      }))
    }
  }
}