// @ts-check
export default class Vector {

  constructor(x, y) {
    if ("number" != typeof x || "number" != typeof y) throw new TypeError(`Expecting 2 number type. Instead, you passed a vector of ( ${typeof x}, ${typeof y} )`)
    this.x = x
    this.y = y
  }

  sub(v) {
    if (v instanceof Vector) { this.x -= v.x, this.y -= v.y
      return this}
    if (typeof v === 'number') {this.x -= v, this.y -= v
      return this}
    throw new TypeError('Expecting either a number or a Vector type')
  }

  add(v) {
    if (v instanceof Vector) { this.x += v.x, this.y += v.y
      return this}
    if (typeof v === 'number') {this.x += v, this.y += v
      return this}
    throw new TypeError('Expecting either a number or a Vector type')
  }

  mult(v) {
    if (v instanceof Vector) { this.x *= v.x, this.y *= v.y
      return this}
    if (typeof v === 'number') {this.x *= v, this.y *= v
      return this}
    throw new TypeError('Expecting either a number or a Vector type')
  }

  dist(v) {
    const distX = v.x - this.x
    const distY = v.y - this.y
    return Math.sqrt(Math.pow(distX, 2) + Math.pow(distY, 2))
  }

  mag() {
    return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2))
  }

  normalize() {
    const e = this.mag();
    return 0 !== e && this.mult(1 / e), this
  }

  // @ts-ignore
  static #checkType(v1, v2) {
    if (!(v1 instanceof Vector)) throw new TypeError("expecting first paremeter to be a Vector object")
    if (!(v2 instanceof Vector) || "number" != typeof v2) throw new TypeError("expecting second paremeter to be either a Vector object or a number")
    return typeof v2
  }

  static sub(v1, v2) {
    if (Vector.#checkType(v1, v2) === 'number') {
      return new Vector(v1.x - v2, v1.y - v2)
    }
    return  new Vector(v1.x - v2.x, v1.y - v2.y)
  }

   static add(v1, v2) {
    if (Vector.#checkType(v1, v2) === 'number') {
      return new Vector(v1.x + v2, v1.y + v2)
    }
    return  new Vector(v1.x + v2.x, v1.y + v2.y)
  }

   static mult(v1, v2) {
    if (Vector.#checkType(v1, v2) === 'number') {
      return new Vector(v1.x * v2, v1.y * v2)
    }
    return  new Vector(v1.x * v2.x, v1.y * v2.y)
  }
  
  static dist(v1, v2) {
    const distX = v1.x - v2.x
    const distY = v1.y - v2.y
    return Math.sqrt(Math.pow(distX, 2) + Math.pow(distY, 2))
  }
}